package com.demo.chatapp.service;

import com.demo.chatapp.model.ChatEntity;
import com.demo.chatapp.web.form.ChatForm;

public interface ChatService {

    ChatEntity findChat(Long id);

    void createChat(ChatForm form, String creator);

    void removeParticipant(Long chatId, String username, String loggedUser) throws Exception;
}

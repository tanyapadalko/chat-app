package com.demo.chatapp.service;

import com.demo.chatapp.model.UserEntity;
import com.demo.chatapp.web.form.UserForm;

import java.util.List;

public interface UserService {

    void createUser(UserForm userForm);

    UserEntity findUser(String username);

    List<UserEntity> findAll();

    List<UserEntity> findAllByUsernameIn(List<String> usernames);
}

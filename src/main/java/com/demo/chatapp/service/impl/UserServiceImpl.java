package com.demo.chatapp.service.impl;

import com.demo.chatapp.model.UserEntity;
import com.demo.chatapp.repository.UserRepository;
import com.demo.chatapp.service.UserService;
import com.demo.chatapp.web.form.UserForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    //TODO check if username is already exists
    @Override
    @Transactional
    public void createUser(UserForm userForm) {
        UserEntity userEntity = new UserEntity(userForm);
        userRepository.save(userEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public UserEntity findUser(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public List<UserEntity> findAll() {
        List<UserEntity> users = userRepository.findAll();
        return users;
    }

    @Override
    public List<UserEntity> findAllByUsernameIn(List<String> usernames) {
        return userRepository.findAllByUsernameIn(usernames);
    }
}

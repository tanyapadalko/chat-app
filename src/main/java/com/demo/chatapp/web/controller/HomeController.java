package com.demo.chatapp.web.controller;

import com.demo.chatapp.model.UserEntity;
import com.demo.chatapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.List;

@Controller
public class HomeController {

    @Autowired
    private UserService userService;

    @GetMapping("/")
    public ModelAndView home(Principal principal) {
        List<UserEntity> users = userService.findAll();
        ModelAndView mav;
        if (principal != null) {
            mav = new ModelAndView("dashboard");
            mav.addObject("loggedUser", principal.getName());
            mav.addObject("users", users);
        } else {
            mav = new ModelAndView("index");
        }
        return mav;
    }


    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/access-denied")
    public String accessDenied() {
        return "/error/access-denied";
    }
}

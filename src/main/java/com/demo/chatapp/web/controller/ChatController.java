package com.demo.chatapp.web.controller;

import com.demo.chatapp.model.ChatEntity;
import com.demo.chatapp.service.ChatService;
import com.demo.chatapp.service.MessageService;
import com.demo.chatapp.web.form.ChatForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequestMapping("/chat")
public class ChatController {

    private MessageService messageService;
    private ChatService chatService;

    @Autowired
    public ChatController(MessageService messageService, ChatService chatService) {
        this.messageService = messageService;
        this.chatService = chatService;
    }

    //TODO deleteChat, addPeopleToChat

    @GetMapping("/new-chat")
    public String createChat(Model model) {
        model.addAttribute("chat", new ChatForm());
        return "chat/create-chat";
    }

    @PostMapping("/create")
    public String saveChat(@Valid @ModelAttribute ChatForm chat, Principal principal,
                           BindingResult errors, Model model) {
        model.addAttribute("chat", chat);
        chatService.createChat(chat, principal.getName());
        return "chat/active-chat";
    }


    @GetMapping(value = "/{chatId}")
    public String openChat(@PathVariable Long chatId, Model model) {
        ChatEntity activeChat = chatService.findChat(chatId);
        model.addAttribute("chat", activeChat);
        model.addAttribute("messages", messageService.findAllMessagesByChatId(chatId));
        model.addAttribute("channels", null);
        return "chat/active-chat";
    }


}

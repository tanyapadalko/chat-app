package com.demo.chatapp.web.controller;

import com.demo.chatapp.service.UserService;
import com.demo.chatapp.web.form.UserForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/user")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping("/signup")
    public String addUser(Model model) {
        model.addAttribute("user", new UserForm());
        return "add-user";
    }

    @PostMapping(value = "/create")
    public String createUser(@Valid @ModelAttribute UserForm user, BindingResult errors, Model model) {
        model.addAttribute("user", user);
        userService.createUser(user);
        return ((errors.hasErrors()) ? "add-user" : "login");
    }

}

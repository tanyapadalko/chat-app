package com.demo.chatapp.service.impl;

import com.demo.chatapp.model.ChatEntity;
import com.demo.chatapp.model.MessageEntity;
import com.demo.chatapp.model.UserEntity;
import com.demo.chatapp.repository.MessageRepository;
import com.demo.chatapp.service.ChatService;
import com.demo.chatapp.service.MessageService;
import com.demo.chatapp.service.UserService;
import com.demo.chatapp.web.form.MessageForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("messageService")
public class MessageServiceImpl implements MessageService {

    private MessageRepository messageRepository;
    private UserService userService;
    private ChatService chatService;

    @Autowired
    public MessageServiceImpl(MessageRepository messageRepository, UserService userService, ChatService chatService) {
        this.messageRepository = messageRepository;
        this.userService = userService;
        this.chatService = chatService;
    }


    @Override
    @Transactional
    public void sendMessage(MessageForm messageForm, String senderUsername) {
        ChatEntity chat = chatService.findChat(messageForm.getChatId());

        MessageEntity newMessage = new MessageEntity();
        newMessage.setMessage(messageForm.getText());
        newMessage.setSender(userService.findUser(senderUsername));
        newMessage.setChat(chat);
        messageRepository.save(newMessage);
        sendMessage(newMessage, null);
    }

    private void sendMessage(MessageEntity message, List<UserEntity> receivers) {
        //TODO implement socket connection and send message to all chat participants
    }


    @Override
    @Transactional(readOnly = true)
    public List<MessageEntity> findAllMessagesByChatId(Long chatId) {
        return messageRepository.findAllByChatId(chatId);
    }


}

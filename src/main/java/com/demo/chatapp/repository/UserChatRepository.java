package com.demo.chatapp.repository;

import com.demo.chatapp.model.UserChatEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserChatRepository extends JpaRepository<UserChatEntity, Long> {
}

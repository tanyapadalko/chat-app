package com.demo.chatapp.service.impl;

import com.demo.chatapp.model.ChatEntity;
import com.demo.chatapp.model.UserChatEntity;
import com.demo.chatapp.model.UserEntity;
import com.demo.chatapp.repository.ChatRepository;
import com.demo.chatapp.repository.UserChatRepository;
import com.demo.chatapp.service.ChatService;
import com.demo.chatapp.service.UserService;
import com.demo.chatapp.web.form.ChatForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service("chatService")
public class ChatServiceImpl implements ChatService {

    private ChatRepository chatRepository;
    private UserService userService;
    private UserChatRepository userChatRepository;

    @Autowired
    public ChatServiceImpl(ChatRepository chatRepository, UserService userService, UserChatRepository userChatRepository) {
        this.chatRepository = chatRepository;
        this.userService = userService;
        this.userChatRepository = userChatRepository;
    }


    @Override
    @Transactional(readOnly = true)
    public ChatEntity findChat(Long id) {
        return chatRepository.findOne(id);
    }

    @Override
    @Transactional
    public void createChat(ChatForm form, String creator) {
        ChatEntity chat = new ChatEntity();
        chat.setCreationDate(LocalDateTime.now());
        chat.setName(form.getDisplayName().isEmpty() ? "chat" : form.getDisplayName());
        chat.setCreator(userService.findUser(creator));
        chat.setUserChats(createUserChat(chat, form.getParticipants()));
        chatRepository.save(chat);
    }

    private List<UserChatEntity> createUserChat(ChatEntity chat, List<String> participantUsernames) {
        List<UserChatEntity> result = new ArrayList<>();
        List<UserEntity> participants = userService.findAllByUsernameIn(participantUsernames);
        for (UserEntity user : participants) {
            UserChatEntity userChat = new UserChatEntity(user, chat);
            userChatRepository.save(userChat);
            result.add(userChat);
        }
        return result;
    }

    @Override
    public void removeParticipant(Long chatId, String username, String loggedUser) throws Exception {
        ChatEntity chat = chatRepository.findOne(chatId);
        if (chat.getCreator().getUsername().equals(username))
            removeUser(username, chat);
        else if (username.equals(loggedUser))
            removeUser(username, chat);
        else
            throw new Exception("You are not authorized to remove other users from this chat");

    }

    private void removeUser(String username, ChatEntity chat) {
        //TODO add removing user from channel
    }

}

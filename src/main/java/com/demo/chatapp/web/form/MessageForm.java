package com.demo.chatapp.web.form;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;


@Getter
@Setter
public class MessageForm extends AbstractForm {

    @Length(min = 1, max = 140)
    private String text;
    @NotNull
    private Long chatId;

}

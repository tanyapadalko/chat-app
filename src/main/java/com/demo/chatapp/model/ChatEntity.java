package com.demo.chatapp.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Entity(name = "CHAT")
public class ChatEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name; //chat name
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "chat")
    private List<UserChatEntity> userChats;
    private LocalDateTime creationDate;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "creator_id")
    private UserEntity creator;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "chat")
    private List<MessageEntity> messages;


}

package com.demo.chatapp.service;

import com.demo.chatapp.model.MessageEntity;
import com.demo.chatapp.web.form.MessageForm;

import java.util.List;

public interface MessageService {

    void sendMessage(MessageForm messageForm, String senderUsername);

    List<MessageEntity> findAllMessagesByChatId(Long chatId);

}

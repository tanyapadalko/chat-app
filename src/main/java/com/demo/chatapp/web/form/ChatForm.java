package com.demo.chatapp.web.form;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ChatForm extends AbstractForm {

    private String displayName;
    private List<String> participants;
    private String creator;
}
